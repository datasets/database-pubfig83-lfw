# PubFig83 + LFW Dataset

The PubFig83 + LFW dataset is the combination of PubFig83 and the LFW 
datasets to form a new benchmark dataset for open-universe face identification. 

Based on the realistic scenarios of automatically searching for people in web 
photos or tagging friends and family in personal photo albums, the purpose of 
the dataset is to allow algorithms to find and identity some individuals while 
ignoring all others as background, or distractor, faces. 

This mimics many real-world applications where face recognition needs to ignore 
many background faces that appear in photos, but are not relevant to the user. 

PubFig83+LFW has 13,002 faces representing 83 individuals from PubFig83, 
divided into 2/3 training (8720 faces) and 1/3 testing set (4,282  faces). 

From LFW, 12,066 faces representing over 5,000 images are used as a distractor set.

## Download Dataset

The PubFig83+LFW dataset comes in several parts:

1. [Matlab Face Recognition Toolbox](http://www.enriquegortiz.com/publications/FaceRecognitionToolbox.zip): A matlab toolbox that has our algorithm LASRC along with many other recent face recognition algorithms and an extensible way to plug in your own algorithm for evaluation. NOTE: The toolbox is slightly upgraded from what was used to generate the paper results (in the paper we used a decimation before PCA which is omitted in this version of the toolbox), which yields slightly better results on all algorithms.
    
2. [Aligned Face Images](http://www.enriquegortiz.com/publications/pf83lfw.zip) (409 MB): Faces from PubFig83 and LFW, organized into directory structures of train, test, and distract. Faces are are pre-aligned by the eye positions as reported by PittPatt-reported fiducials and filtered to reduce the chance of algorithms not finding the face suitable (i.e. only faces that PittPatt, SHORE, Google Picasa, MS Photo Gallery, and Apple iPhoto found were kept).
    
3. [Features Vectors](http://briancbecker.com/files/downloads/pubfig83lfw/pubfig83lfw_features.zip) (182 MB): HOG, LBP, and Gabor wavelet features extracted from the aligned face images, concatenated, and reduced to 2048 dimensions with PCA. For our paper, we only used the first 1536 dimensions of the descriptors.
    
4. [Raw Face Images](http://briancbecker.com/files/downloads/pubfig83lfw/pubfig83lfw_raw_in_dirs.zip) (381 MB): minimally modified faces from PubFig83 and LFW. Faces have been resized to 250×250 and filtered to reduce the chance of algorithms not finding the face suitable (i.e. only faces that PittPatt, SHORE, Google Picasa, MS Photo Gallery, and Apple iPhoto found were kept). Note PubFig and LFW use different cropping, so you must perform your own processing to bring the datasets into alignment.




**Reported Results** at Brian C. Becker's [website](http://www.briancbecker.com/blog/research/pubfig83-lfw-dataset/).
